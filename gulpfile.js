var gulp = require('gulp');
var browserSync = require('browser-sync').create();
var typescript = require('gulp-typescript');
var sourcemaps = require('gulp-sourcemaps');

var tsProject = typescript.createProject('./tsconfig.json');
var tsconfig = require('./tsconfig.json');

var config = {
  root: './',
  outFile: tsconfig.outFifle,
  srcFiles: [
    'src/**/*.ts',
  ]
};

gulp.task('default', ['build']);

gulp.task('build', function () {
  return gulp.src(config.srcFiles)
    .pipe(sourcemaps.init())
    .pipe(tsProject())
    .pipe(sourcemaps.write(config.root))
    .pipe(gulp.dest(config.root))
    .pipe(browserSync.stream());
});

gulp.task('watch', ['build'], function () {
  browserSync.init({
    server: '.'
  });

  gulp.watch(config.srcFiles, ['build']);
});